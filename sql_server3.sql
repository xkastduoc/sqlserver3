CREATE TABLE marca (
  idmarca INTEGER   NOT NULL ,
  nombre VARCHAR(50)    ,
  provedor VARCHAR(50)      ,
PRIMARY KEY(idmarca));




CREATE TABLE vendedor (
  idvendedor INTEGER   NOT NULL ,
  rut VARCHAR(20)    ,
  nombre VARCHAR(50)    ,
  apellido VARCHAR(50)      ,
PRIMARY KEY(idvendedor));




CREATE TABLE boleta (
  idboleta INTEGER   NOT NULL ,
  fecha DATE    ,
  vendedor_idvendedor INTEGER   NOT NULL   ,
PRIMARY KEY(idboleta)  ,
  FOREIGN KEY(vendedor_idvendedor)
    REFERENCES vendedor(idvendedor));


CREATE INDEX boleta_FKIndex1 ON boleta (vendedor_idvendedor);


CREATE INDEX IFK_Rel_01 ON boleta (vendedor_idvendedor);


CREATE TABLE producto (
  idproducto INTEGER   NOT NULL ,
  marca_idmarca INTEGER   NOT NULL ,
  descripcion VARCHAR(50)    ,
  precio INTEGER    ,
  stock INTEGER      ,
PRIMARY KEY(idproducto)  ,
  FOREIGN KEY(marca_idmarca)
    REFERENCES marca(idmarca));


CREATE INDEX producto_FKIndex1 ON producto (marca_idmarca);


CREATE INDEX IFK_Rel_04 ON producto (marca_idmarca);


CREATE TABLE detalle_boleta (
  iddetalle_boleta INTEGER   NOT NULL ,
  boleta_idboleta INTEGER   NOT NULL ,
  producto_idproducto INTEGER   NOT NULL ,
  cantidad INTEGER    ,
  precio_unitario INTEGER      ,
PRIMARY KEY(iddetalle_boleta)    ,
  FOREIGN KEY(producto_idproducto)
    REFERENCES producto(idproducto),
  FOREIGN KEY(boleta_idboleta)
    REFERENCES boleta(idboleta));


CREATE INDEX detalle_boleta_FKIndex1 ON detalle_boleta (producto_idproducto);
CREATE INDEX detalle_boleta_FKIndex2 ON detalle_boleta (boleta_idboleta);


CREATE INDEX IFK_Rel_02 ON detalle_boleta (producto_idproducto);
CREATE INDEX IFK_Rel_03 ON detalle_boleta (boleta_idboleta);



