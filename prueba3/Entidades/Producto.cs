﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Producto
    {
        public string id { get; set; }
        public string cantidad { get; set; }
        public string precio { get; set; }
    }
}
