﻿namespace prueba3
{
    partial class Inicio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.procesoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearVendedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarVendedorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarVendedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarVendedoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.crearMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editarMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listarMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eliminarMarcaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ventaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inventarioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.procesoToolStripMenuItem,
            this.marcaToolStripMenuItem,
            this.ventaToolStripMenuItem,
            this.inventarioToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // procesoToolStripMenuItem
            // 
            this.procesoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearVendedorToolStripMenuItem,
            this.editarVendedorToolStripMenuItem,
            this.listarVendedoresToolStripMenuItem,
            this.eliminarVendedoresToolStripMenuItem});
            this.procesoToolStripMenuItem.Name = "procesoToolStripMenuItem";
            this.procesoToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.procesoToolStripMenuItem.Text = "Proceso";
            // 
            // crearVendedorToolStripMenuItem
            // 
            this.crearVendedorToolStripMenuItem.Name = "crearVendedorToolStripMenuItem";
            this.crearVendedorToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.crearVendedorToolStripMenuItem.Text = "Crear Vendedor";
            this.crearVendedorToolStripMenuItem.Click += new System.EventHandler(this.crearVendedorToolStripMenuItem_Click);
            // 
            // editarVendedorToolStripMenuItem
            // 
            this.editarVendedorToolStripMenuItem.Name = "editarVendedorToolStripMenuItem";
            this.editarVendedorToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.editarVendedorToolStripMenuItem.Text = "Editar Vendedor";
            this.editarVendedorToolStripMenuItem.Click += new System.EventHandler(this.editarVendedorToolStripMenuItem_Click);
            // 
            // listarVendedoresToolStripMenuItem
            // 
            this.listarVendedoresToolStripMenuItem.Name = "listarVendedoresToolStripMenuItem";
            this.listarVendedoresToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.listarVendedoresToolStripMenuItem.Text = "Listar Vendedores";
            this.listarVendedoresToolStripMenuItem.Click += new System.EventHandler(this.listarVendedoresToolStripMenuItem_Click);
            // 
            // eliminarVendedoresToolStripMenuItem
            // 
            this.eliminarVendedoresToolStripMenuItem.Name = "eliminarVendedoresToolStripMenuItem";
            this.eliminarVendedoresToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.eliminarVendedoresToolStripMenuItem.Text = "Eliminar Vendedores";
            this.eliminarVendedoresToolStripMenuItem.Click += new System.EventHandler(this.eliminarVendedoresToolStripMenuItem_Click);
            // 
            // marcaToolStripMenuItem
            // 
            this.marcaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.crearMarcaToolStripMenuItem,
            this.editarMarcaToolStripMenuItem,
            this.listarMarcaToolStripMenuItem,
            this.eliminarMarcaToolStripMenuItem});
            this.marcaToolStripMenuItem.Name = "marcaToolStripMenuItem";
            this.marcaToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.marcaToolStripMenuItem.Text = "Marca";
            // 
            // crearMarcaToolStripMenuItem
            // 
            this.crearMarcaToolStripMenuItem.Name = "crearMarcaToolStripMenuItem";
            this.crearMarcaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.crearMarcaToolStripMenuItem.Text = "Crear Marca";
            this.crearMarcaToolStripMenuItem.Click += new System.EventHandler(this.crearMarcaToolStripMenuItem_Click);
            // 
            // editarMarcaToolStripMenuItem
            // 
            this.editarMarcaToolStripMenuItem.Name = "editarMarcaToolStripMenuItem";
            this.editarMarcaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.editarMarcaToolStripMenuItem.Text = "Editar Marca";
            this.editarMarcaToolStripMenuItem.Click += new System.EventHandler(this.editarMarcaToolStripMenuItem_Click);
            // 
            // listarMarcaToolStripMenuItem
            // 
            this.listarMarcaToolStripMenuItem.Name = "listarMarcaToolStripMenuItem";
            this.listarMarcaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.listarMarcaToolStripMenuItem.Text = "Listar Marca";
            this.listarMarcaToolStripMenuItem.Click += new System.EventHandler(this.listarMarcaToolStripMenuItem_Click);
            // 
            // eliminarMarcaToolStripMenuItem
            // 
            this.eliminarMarcaToolStripMenuItem.Name = "eliminarMarcaToolStripMenuItem";
            this.eliminarMarcaToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.eliminarMarcaToolStripMenuItem.Text = "Eliminar Marca";
            this.eliminarMarcaToolStripMenuItem.Click += new System.EventHandler(this.eliminarMarcaToolStripMenuItem_Click);
            // 
            // ventaToolStripMenuItem
            // 
            this.ventaToolStripMenuItem.Name = "ventaToolStripMenuItem";
            this.ventaToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.ventaToolStripMenuItem.Text = "Venta";
            this.ventaToolStripMenuItem.Click += new System.EventHandler(this.ventaToolStripMenuItem_Click);
            // 
            // inventarioToolStripMenuItem
            // 
            this.inventarioToolStripMenuItem.Name = "inventarioToolStripMenuItem";
            this.inventarioToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.inventarioToolStripMenuItem.Text = "Inventario";
            this.inventarioToolStripMenuItem.Click += new System.EventHandler(this.inventarioToolStripMenuItem_Click);
            // 
            // Inicio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Inicio";
            this.Text = "Inicio";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem procesoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearVendedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarVendedorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarVendedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarVendedoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem marcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem crearMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editarMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listarMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eliminarMarcaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ventaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inventarioToolStripMenuItem;
    }
}