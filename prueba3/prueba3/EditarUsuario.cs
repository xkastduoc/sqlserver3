﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class EditarUsuario : Form
    {
        public EditarUsuario()
        {
            InitializeComponent();
            pnModificar.Hide();
            txtID.Hide();
        }

        private void Nombre_Click(object sender, EventArgs e)
        {

        }

        private void Buscar_Click(object sender, EventArgs e)
        {
            string rutModificar = txtRUT.Text;

            try
            {
                DataTable datosVendedor = VendedorBCP.BuscarVendedor(rutModificar);
                if (datosVendedor.Rows.Count > 0)
                {
                    txtRUTMod.Text = datosVendedor.Rows[0][1].ToString();
                    txtNombre.Text = datosVendedor.Rows[0][2].ToString();
                    txtApellido.Text = datosVendedor.Rows[0][3].ToString();
                    txtID.Text= datosVendedor.Rows[0][0].ToString();
                }

                pnModificar.Show();

            }
            catch(Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al buscar el Vendedor");
            }
            
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string rut = txtRUTMod.Text;
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;
                string id = txtID.Text;


                VendedorBCP.EditarVendedor(id,rut, nombre, apellido);

                MessageBox.Show("Vendedor modificado con exito");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al modificar el vendedor");
            }
        }
    }
}
