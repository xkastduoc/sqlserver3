﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class ListarMarca : Form
    {
        public ListarMarca()
        {
            InitializeComponent();
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Marca", "Marca");
            dataGridView1.Columns.Add("Proveedor", "Proveedor");

            try
            {
                DataTable datosVendedor = MarcaBCP.ListarMarca();

                for (int i = 0; i < datosVendedor.Rows.Count; i++)
                {
                    dataGridView1.Rows.Add(datosVendedor.Rows[i][0].ToString(), datosVendedor.Rows[i][1].ToString(), datosVendedor.Rows[i][2].ToString());

                }


            }
            catch (Exception ex)
            {

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
