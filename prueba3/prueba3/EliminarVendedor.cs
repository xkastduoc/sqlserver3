﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class EliminarVendedor : Form
    {
        public EliminarVendedor()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string rut = txtRut.Text;



                VendedorBCP.EliminarVendedor(rut);
                LimpiarParametros();

                MessageBox.Show("Vendedor creado con exito");
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al eliminar el vendedor");
            }
        }

        private void LimpiarParametros()
        {
            txtRut.Text = "";
        }
    }
}
