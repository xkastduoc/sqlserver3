﻿namespace prueba3
{
    partial class EditarUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtRUT = new System.Windows.Forms.TextBox();
            this.Buscar = new System.Windows.Forms.Button();
            this.pnModificar = new System.Windows.Forms.Panel();
            this.txtRUTMod = new System.Windows.Forms.TextBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.txtApellido = new System.Windows.Forms.TextBox();
            this.RUT = new System.Windows.Forms.Label();
            this.Nombre = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnModificar = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.pnModificar.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(54, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ingrese RUT de Vendedor";
            // 
            // txtRUT
            // 
            this.txtRUT.Location = new System.Drawing.Point(223, 30);
            this.txtRUT.Name = "txtRUT";
            this.txtRUT.Size = new System.Drawing.Size(100, 20);
            this.txtRUT.TabIndex = 1;
            // 
            // Buscar
            // 
            this.Buscar.Location = new System.Drawing.Point(347, 30);
            this.Buscar.Name = "Buscar";
            this.Buscar.Size = new System.Drawing.Size(75, 23);
            this.Buscar.TabIndex = 2;
            this.Buscar.Text = "Buscar";
            this.Buscar.UseVisualStyleBackColor = true;
            this.Buscar.Click += new System.EventHandler(this.Buscar_Click);
            // 
            // pnModificar
            // 
            this.pnModificar.Controls.Add(this.txtID);
            this.pnModificar.Controls.Add(this.btnModificar);
            this.pnModificar.Controls.Add(this.label4);
            this.pnModificar.Controls.Add(this.Nombre);
            this.pnModificar.Controls.Add(this.RUT);
            this.pnModificar.Controls.Add(this.txtApellido);
            this.pnModificar.Controls.Add(this.txtNombre);
            this.pnModificar.Controls.Add(this.txtRUTMod);
            this.pnModificar.Location = new System.Drawing.Point(69, 100);
            this.pnModificar.Name = "pnModificar";
            this.pnModificar.Size = new System.Drawing.Size(403, 214);
            this.pnModificar.TabIndex = 3;
            // 
            // txtRUTMod
            // 
            this.txtRUTMod.Location = new System.Drawing.Point(154, 23);
            this.txtRUTMod.Name = "txtRUTMod";
            this.txtRUTMod.Size = new System.Drawing.Size(100, 20);
            this.txtRUTMod.TabIndex = 0;
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(154, 66);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(100, 20);
            this.txtNombre.TabIndex = 1;
            // 
            // txtApellido
            // 
            this.txtApellido.Location = new System.Drawing.Point(154, 112);
            this.txtApellido.Name = "txtApellido";
            this.txtApellido.Size = new System.Drawing.Size(100, 20);
            this.txtApellido.TabIndex = 2;
            // 
            // RUT
            // 
            this.RUT.AutoSize = true;
            this.RUT.Location = new System.Drawing.Point(70, 29);
            this.RUT.Name = "RUT";
            this.RUT.Size = new System.Drawing.Size(30, 13);
            this.RUT.TabIndex = 3;
            this.RUT.Text = "RUT";
            // 
            // Nombre
            // 
            this.Nombre.AutoSize = true;
            this.Nombre.Location = new System.Drawing.Point(70, 73);
            this.Nombre.Name = "Nombre";
            this.Nombre.Size = new System.Drawing.Size(44, 13);
            this.Nombre.TabIndex = 4;
            this.Nombre.Text = "Nombre";
            this.Nombre.Click += new System.EventHandler(this.Nombre_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(70, 115);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Apellido";
            // 
            // btnModificar
            // 
            this.btnModificar.Location = new System.Drawing.Point(262, 159);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModificar.TabIndex = 6;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(30, 178);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 7;
            // 
            // EditarUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 367);
            this.Controls.Add(this.pnModificar);
            this.Controls.Add(this.Buscar);
            this.Controls.Add(this.txtRUT);
            this.Controls.Add(this.label1);
            this.Name = "EditarUsuario";
            this.Text = "EditarUsuario";
            this.pnModificar.ResumeLayout(false);
            this.pnModificar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRUT;
        private System.Windows.Forms.Button Buscar;
        private System.Windows.Forms.Panel pnModificar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label Nombre;
        private System.Windows.Forms.Label RUT;
        private System.Windows.Forms.TextBox txtApellido;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.TextBox txtRUTMod;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.TextBox txtID;
    }
}