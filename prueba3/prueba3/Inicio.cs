﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace prueba3
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void crearVendedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearUsuario frmCrearUsuario = new CrearUsuario();
            frmCrearUsuario.Show();
        }

        private void editarVendedorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditarUsuario frmEditarUsuario = new EditarUsuario();
            frmEditarUsuario.Show();
        }

        private void listarVendedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListarVendedor frmListarVendedor = new ListarVendedor();
            frmListarVendedor.Show();
        }

        private void eliminarVendedoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarVendedor frmEliminarVendedor = new EliminarVendedor();
            frmEliminarVendedor.Show();
        }

        private void crearMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CrearMarca frmCrearUsuario = new CrearMarca();
            frmCrearUsuario.Show();
        }

        private void editarMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EditarMarca frmEditarUsuario = new EditarMarca();
            frmEditarUsuario.Show();
        }

        private void listarMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListarMarca frmListarVendedor = new ListarMarca();
            frmListarVendedor.Show();
        }

        private void eliminarMarcaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarMarca frmEliminarVendedor = new EliminarMarca();
            frmEliminarVendedor.Show();
        }

        private void ventaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Venta frmEliminarVendedor = new Venta();
            frmEliminarVendedor.Show();
        }

        private void inventarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Inventario frmEliminarVendedor = new Inventario();
            frmEliminarVendedor.Show();
        }
    }
}
