﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace prueba3
{
    public partial class EliminarMarca : Form
    {
        public EliminarMarca()
        {
            InitializeComponent();
            DataTable dtMarca = new DataTable();
            dtMarca = BCP.MarcaBCP.ListarMarca();
            DataRow rowMarca;
            rowMarca = dtMarca.NewRow();
            rowMarca[0] = "0";
            rowMarca[1] = "Seleccione Marca";
            dtMarca.Rows.InsertAt(rowMarca, 0);
            cmMarca.DataSource = dtMarca;
            cmMarca.DisplayMember = "nombre";
            cmMarca.ValueMember = "nombre";
            cmMarca.SelectedItem = 0;
            cmMarca.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                
                string marca = cmMarca.SelectedValue.ToString();



                BCP.MarcaBCP.EliminarMarca(marca);

                MessageBox.Show("Vendedor creado con exito");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al eliminar el vendedor");
            }
        }

        
    }
}
