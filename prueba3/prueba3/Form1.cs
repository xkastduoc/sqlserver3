﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void logear_Click(object sender, EventArgs e)
        {
            DataTable dtsRetorno = new DataTable();

            try { 
                dtsRetorno = BCP.LoginBCP.loginUsuario(usuario.Text, contrasenia.Text);
                if (dtsRetorno.Rows.Count > 0)
                {
                    Inicio frmInicio = new Inicio();
                    frmInicio.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Login NOK");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al ingresar");
            }
        }
    }
}
