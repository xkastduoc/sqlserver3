﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;

namespace prueba3
{
    public partial class Inventario : Form
    {
        static Producto producto = new Producto();
        public Inventario()
        {
            InitializeComponent();
            DataTable dtProducto = new DataTable();
            dtProducto = BCP.ProductoBCP.ListarMarca();
            DataRow rowProducto;
            rowProducto = dtProducto.NewRow();
            rowProducto[0] = "0";
            rowProducto[1] = "Seleccione Producto";
            dtProducto.Rows.InsertAt(rowProducto, 0);
            cmProducto.DataSource = dtProducto;
            cmProducto.DisplayMember = "descripcion";
            cmProducto.ValueMember = "idproducto";
            cmProducto.SelectedItem = 0;
            cmProducto.DropDownStyle = ComboBoxStyle.DropDownList;
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Descripcion", "Descripcion");
            dataGridView1.Columns.Add("Nombre", "Nombre");
            dataGridView1.Columns.Add("Stock", "Stock");
        }

        private void btBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable datosVendedor = BCP.ProductoBCP.BuscarProductoStock(cmProducto.SelectedValue.ToString());
                if (datosVendedor.Rows.Count > 0)
                {
                    dataGridView1.Rows.Add(datosVendedor.Rows[0][0].ToString(), datosVendedor.Rows[0][1].ToString(), datosVendedor.Rows[0][2].ToString(), datosVendedor.Rows[0][3].ToString());
                    
                    producto.id = datosVendedor.Rows[0][0].ToString();
                    producto.cantidad = datosVendedor.Rows[0][3].ToString();

               


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al buscar el Vendedor");
            }
        }

        private void btRebajar_Click(object sender, EventArgs e)
        {
            string cantidadRebaje = txtCantidad.Text;
            try { 
            if (Convert.ToInt32(producto.cantidad) >= Convert.ToInt32(cantidadRebaje))
            {
                BCP.ProductoBCP.Inventario(cmProducto.SelectedValue.ToString(), txtCantidad.Text);
                dataGridView1.Rows.Clear();
                    MessageBox.Show("Stock actualizado con exito");
                    txtCantidad.Text = "";

                    DataTable dtProducto = new DataTable();
                    dtProducto = BCP.ProductoBCP.ListarMarca();
                    DataRow rowProducto;
                    rowProducto = dtProducto.NewRow();
                    rowProducto[0] = "0";
                    rowProducto[1] = "Seleccione Producto";
                    dtProducto.Rows.InsertAt(rowProducto, 0);
                    cmProducto.DataSource = dtProducto;
                    cmProducto.DisplayMember = "descripcion";
                    cmProducto.ValueMember = "idproducto";
                    cmProducto.SelectedItem = 0;
                    cmProducto.DropDownStyle = ComboBoxStyle.DropDownList;

                }
            else
            {
                MessageBox.Show("La cantidad que sea rebajar sel stock es mayor al stock");
            }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al actualizar el Stock");
            }
        }
    }
}
