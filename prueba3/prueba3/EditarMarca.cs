﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class EditarMarca : Form
    {
        public EditarMarca()
        {
            InitializeComponent();
            pnModificar.Hide();
            txtID.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string marcaBuscar = txtMarca.Text;

            try
            {
                DataTable datosVendedor = MarcaBCP.BuscarMarca(marcaBuscar);
                if (datosVendedor.Rows.Count > 0)
                {
                    txtNombre.Text = datosVendedor.Rows[0][1].ToString();
                    txtProveedor.Text = datosVendedor.Rows[0][2].ToString();
                    txtID.Text = datosVendedor.Rows[0][0].ToString();
                }

                pnModificar.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al buscar el Vendedor");
            }

        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                string nombre = txtMarca.Text;
                string proveedor = txtProveedor.Text;
                string id = txtID.Text;

                MarcaBCP.EditarMarca(id,nombre,proveedor);

                MessageBox.Show("Vendedor modificado con exito");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al modificar el vendedor");
            }
        }
    }
}
