﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class CrearMarca : Form
    {
        public CrearMarca()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string marca = txtMarca.Text;
                string provedor = txtProveedor.Text;



                MarcaBCP.crearMarca(marca,provedor);
                LimpiarParametros();

                MessageBox.Show("Marca creada con exito");

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al crear la Marca");
            }
        }

        private void LimpiarParametros()
        {
            txtMarca.Text = "";
            txtProveedor.Text = "";
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
