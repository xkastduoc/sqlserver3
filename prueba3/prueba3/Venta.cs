﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidades;

namespace prueba3
{
    public partial class Venta : Form
    {
        static List<Producto> listaProducto = new List<Producto>();
        static int totalInt;
        static string total;
        public Venta()
        {
            InitializeComponent();
            DataTable dtProducto = new DataTable();
            dtProducto = BCP.ProductoBCP.ListarMarca();
            DataRow rowProducto;
            rowProducto = dtProducto.NewRow();
            rowProducto[0] = "0";
            rowProducto[1] = "Seleccione Producto";
            dtProducto.Rows.InsertAt(rowProducto, 0);
            cmProducto.DataSource = dtProducto;
            cmProducto.DisplayMember = "descripcion";
            cmProducto.ValueMember = "idproducto";
            cmProducto.SelectedItem = 0;
            cmProducto.DropDownStyle = ComboBoxStyle.DropDownList;

            DataTable dtVendedor = new DataTable();
            dtProducto = BCP.VendedorBCP.ListarVendedor();
            DataRow rowVendedor;
            rowVendedor = dtProducto.NewRow();
            rowVendedor[0] = "0";
            rowVendedor[1] = "Seleccione Vendedor";
            dtProducto.Rows.InsertAt(rowVendedor, 0);
            cmVendedor.DataSource = dtProducto;
            cmVendedor.DisplayMember = "nombre";
            cmVendedor.ValueMember = "idvendedor";
            cmVendedor.SelectedItem = 0;
            cmVendedor.DropDownStyle = ComboBoxStyle.DropDownList;

            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Descripcion", "Descripcion");
            dataGridView1.Columns.Add("Nombre", "Nombre");
            dataGridView1.Columns.Add("Precio", "Precio");
            dataGridView1.Columns.Add("Cantidad", "Cantidad");
            DateTime fechaHoy = DateTime.Now;

            string fecha = fechaHoy.ToString("d");
            txtFecha.Text = fecha;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btAgregar_Click(object sender, EventArgs e)
        {
            

            try
            {
                DataTable datosVendedor = BCP.ProductoBCP.BuscarProducto(cmProducto.SelectedValue.ToString());
                if (datosVendedor.Rows.Count > 0)
                {
                    dataGridView1.Rows.Add(datosVendedor.Rows[0][0].ToString(), datosVendedor.Rows[0][1].ToString(), datosVendedor.Rows[0][2].ToString(), datosVendedor.Rows[0][3].ToString(),txCantidad.Text);
                    Producto producto = new Producto();
                    producto.id = datosVendedor.Rows[0][0].ToString();
                    producto.cantidad = txCantidad.Text;

                    int precio = Convert.ToInt32(datosVendedor.Rows[0][3].ToString());
                    int cantidad = Convert.ToInt32(txCantidad.Text);

                    producto.precio = (precio * cantidad).ToString();

                    listaProducto.Add(producto);

                    totalInt = totalInt + (Convert.ToInt32(datosVendedor.Rows[0][3].ToString())* Convert.ToInt32(txCantidad.Text));
                    txtTotal.Text = totalInt.ToString();
                }
                

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error al buscar el Vendedor");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            try
            {

                BCP.ProductoBCP.crearVenta(txtFecha.Text, cmVendedor.SelectedValue.ToString(), txtTotal.Text);

                DataTable datosBoleta = BCP.ProductoBCP.ObtenerBoleta();

                string idBoleta = datosBoleta.Rows[0][0].ToString();

                BCP.ProductoBCP.GuardarDetalleVenta(listaProducto, idBoleta);
                

                MessageBox.Show("Venta realizada con exito");

                DataTable dtProducto = new DataTable();
                dtProducto = BCP.ProductoBCP.ListarMarca();
                DataRow rowProducto;
                rowProducto = dtProducto.NewRow();
                rowProducto[0] = "0";
                rowProducto[1] = "Seleccione Producto";
                dtProducto.Rows.InsertAt(rowProducto, 0);
                cmProducto.DataSource = dtProducto;
                cmProducto.DisplayMember = "descripcion";
                cmProducto.ValueMember = "idproducto";
                cmProducto.SelectedItem = 0;
                cmProducto.DropDownStyle = ComboBoxStyle.DropDownList;

                txtTotal.Text = "";
                
                dataGridView1.Rows.Clear();
                txCantidad.Text = "";

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al guardar la boleta");
            }

        }

        private void txtFecha_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
