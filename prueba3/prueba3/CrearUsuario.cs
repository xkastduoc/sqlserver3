﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BCP;

namespace prueba3
{
    public partial class CrearUsuario : Form
    {
        public CrearUsuario()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string rut = txtRUT.Text;
                string nombre = txtNombre.Text;
                string apellido = txtApellido.Text;



                VendedorBCP.crearVendedor(rut,nombre,apellido);
                LimpiarParametros();

                MessageBox.Show("Vendedor creado con exito");

            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al crear el vendedor");
            }
        }

        private void LimpiarParametros()
        {
            txtRUT.Text = "";
            txtNombre.Text = "";
            txtApellido.Text = "";
        }
    }
}
