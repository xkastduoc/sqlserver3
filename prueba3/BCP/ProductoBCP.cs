﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidades;

namespace BCP
{
    public class ProductoBCP
    {
        public static DataTable ListarMarca()
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                dtsRetorno = sql.querySPDataset("SVC_QRY_LISTARPRODUCTO");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static DataTable BuscarProducto(string id)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@id", id);
                dtsRetorno = sql.querySPDataset("SVC_QRY_BUSCARPRODUCTO");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void crearVenta(string fecha, string idvendedor, string total)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@fecha", fecha);
                sql.addParametro("@idvendedor", idvendedor);
                sql.addParametro("@total", idvendedor);
                sql.ejecutarSPNoReturn("SVC_INS_BOLETA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void GuardarDetalleVenta(List<Producto> listaProducto, string idboleta)
        {
            try
            {
                

                foreach (Producto producto in listaProducto)
                {
                    DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                    sql.addParametro("@idboleta", idboleta);
                    sql.addParametro("@idproducto", producto.id);
                    sql.addParametro("@cantidad", producto.cantidad);
                    sql.addParametro("@preciounitario", producto.precio);
                    sql.ejecutarSPNoReturn("SVC_INS_DETALLE_BOLETA");
                    sql.Desconectar();
                }
                
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable  ObtenerBoleta()
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                dtsRetorno = sql.querySPDataset("SVC_QRY_BOLETA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }


        public static DataTable BuscarProductoStock(string id)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@id", id);
                dtsRetorno = sql.querySPDataset("SVC_QRY_BUSCARPRODUCTOSTOCK");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void Inventario(string id, string stock)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@id", id);
                sql.addParametro("@stock", stock);
                dtsRetorno = sql.querySPDataset("SVC_UPS_STOCK");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
        }

    }
}
