﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCP
{
    public class MarcaBCP
    {

        public static void crearMarca(string nombre, string provedor)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@nombre", nombre);
                sql.addParametro("@provedor", provedor);
                sql.ejecutarSPNoReturn("SVS_INS_MARCA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable BuscarMarca(string marca)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@marca", marca);
                dtsRetorno = sql.querySPDataset("SVC_QRY_BUSCARMARCA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void EditarMarca(string id,string nombre, string provedor)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();

                sql.addParametro("@idmarca", id);
                sql.addParametro("@nombre_nuevo", nombre);
                sql.addParametro("@provedor_nuevo", provedor);
                sql.ejecutarSPNoReturn("SVS_UPD_MARCA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ListarMarca()
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                dtsRetorno = sql.querySPDataset("SVC_QRY_LISTARMARCA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void EliminarMarca(string nombre)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@nombre", nombre);
                sql.ejecutarSPNoReturn("SVC_DEL_MARCA");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
