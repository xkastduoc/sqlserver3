﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAO;
using System.Data;

namespace BCP
{
    public class LoginBCP
    {
        public static DataTable loginUsuario(string user, string pass)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@usuario", user);
                sql.addParametro("@pass", pass);
                dtsRetorno = sql.querySPDataset("SVC_LOGIN");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }
    }
}
