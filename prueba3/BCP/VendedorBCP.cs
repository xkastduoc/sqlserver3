﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BCP
{
    public class VendedorBCP
    {
        public static void crearVendedor(string rut, string nombre, string apellido)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@rut", rut);
                sql.addParametro("@nombre", nombre);
                sql.addParametro("@apellido", apellido);
                sql.ejecutarSPNoReturn("SVS_INS_VENDEDOR");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable BuscarVendedor(string rut)
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@rut", rut);
                dtsRetorno = sql.querySPDataset("SVC_QRY_BUSCAR");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void EditarVendedor(string id,string rut, string nombre, string apellido)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();

                sql.addParametro("@idvendedor",id );
                sql.addParametro("@rut_nuevo", rut);
                sql.addParametro("@nombre_nuevo", nombre);
                sql.addParametro("@apellido_nuevo", apellido);
                sql.ejecutarSPNoReturn("SVS_UPD_VENDEDOR");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static DataTable ListarVendedor()
        {
            DataSet dtsRetorno = new DataSet();
            try
            {
                //string passwordEncryptado = Util.encriptar(pass);

                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                dtsRetorno = sql.querySPDataset("SVC_QRY_LISTARVENDEDOR");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
            }
            return dtsRetorno.Tables[0];
        }

        public static void EliminarVendedor(string rut)
        {
            try
            {
                DAO.SqlServer.SqlserverDAO sql = new DAO.SqlServer.SqlserverDAO();
                sql.addParametro("@rut", rut);
                sql.ejecutarSPNoReturn("SVS_DEL_VENDEDOR");
                sql.Desconectar();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
